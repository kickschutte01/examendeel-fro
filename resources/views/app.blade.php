@extends('layouts.app')

@section('content')
    <div class="font-sans antialiased" id="app" style="font-family: Heebo">
        <div>
            <div class="hidden lg:block">
                <div class="h-12 flex bg-gray-100">
                    <div class="flex w-full justify-center items-center">
                        <div class="w-full relative overflow-hidden p-5" id="slider">
                            <ul>
                                @foreach($articles as $article)
                                    <li class="w-full mr-24 relative float-left">
                                        <div class="slider-container w-full">
                                            <p class="ml-8 mt-2"><b>{{ $article->title }}</b></p>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="h-16 flex text-l bg-black relative">
                    <div class="flex w-4/6 justify-center">
                        <div class="flex h-full items-center text-white font-bold">
                            <div class="mr-8"><router-link active-class="py-1 border-b-2 border-red-600" :to="{ name:'homepage' }" exact>Homepagina</router-link></div>
                            <div class="mr-8 hover:text-gray-400"><router-link active-class="py-1 border-b-2 border-red-600" :to="{ name:'nieuws' }">Nieuws</router-link></div>
                            <div class="mr-8 hover:text-gray-400"><router-link active-class="py-1 border-b-2 border-red-600" :to="{ name:'verstappen' }">Max Verstappen</router-link></div>
                            <div class="mr-8 hover:text-gray-400"><router-link active-class="py-1 border-b-2 border-red-600" :to="{ name:'standen' }">Standen</router-link></div>
                        </div>
                    </div>
                    <div class="special-div flex flex-row-reverse w-1/6 h-full items-center justify-center bg-red-600">
                        <div class="special-div-content-plus text-white text-3xl">
                            <i class="pr-6 hover:text-gray-400 fab fa-twitter-square"></i>
                            <i class="fab hover:text-gray-400 fa-facebook-square"></i>
                            <i class="pl-6 hover:text-gray-400 fab fa-instagram"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block lg:hidden">
                <navbar></navbar>
            </div>
            <div class="w-full xl:h-100 overflow-hidden">
                <img class="fill-current" src="{{ asset('./images/header.jpg') }}" alt="Header Formule1Vandaag.nl">
            </div>
            <div class="h-full bg-white">
                <div>
                    <router-view></router-view>
                    <vue-footer></vue-footer>
                </div>
            </div>
        </div>
    </div>

    <script src="/js/app.js"></script>

@endsection
