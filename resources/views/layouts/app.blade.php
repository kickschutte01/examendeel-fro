<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="Op deze website vind je alle dagelijkse informatie over de Formule 1, Verstappen, Dutch GP en nog veel meer. Bezoek ons nu!">
    <meta http-equiv="language" content="NL">

	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">

    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Heebo&display=swap" rel="stylesheet">

    <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">

	<title>Formule1Vandaag - @yield('page_title', 'F1 nieuws op een rij!')</title>
</head>
<body>
	@section('content')
	@show

    <style>

        body {
            font-family: 'Heebo', sans-serif;
        }

        .special-div {
            transform:  translateZ(0) skew(-20deg);
            transform-origin: bottom left;

        }

        .special-div-content-plus {
            transform:  translateZ(0) skew(20deg);
        }

        .special-div-content-minus {
            transform:  translateZ(0) skew(-20deg);
        }


        /**** slider ****/
        #slider li
        {
            float: left;
            width: 1800px;
        }

        #slider ul
        {
            position: absolute;
            left: 0px;
            top: 0px;
            width: 9000px;
            -webkit-transition: left .3s linear;
        }

        /*** target hooks ****/

        @-webkit-keyframes slide-animation {
            0% {opacity:0;}
            2% {opacity:1;}
            20% {left: 0px}
            25% {left: -1870px}
            45% {left: -1870px}
            50% {left: -3740px}
            70% {left: -3740px}
            75% {left: -5610px}
            100% {left: -5610px}
        }

        #slider ul
        {
            -webkit-animation: slide-animation 25s infinite;
        }

        /* use to paused the content on mouse over */

        #slider ul:hover
        {
            -moz-animation-play-state: paused;
            -webkit-animation-play-state: paused;
        }

        .gradient::after {
            display: block;
            position: relative;
            background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0) 0, #000 100%);
            margin-top: -150px;
            height: 150px;
            width: 100%;
            content: '';
        }

    </style>

</body>
</html>
