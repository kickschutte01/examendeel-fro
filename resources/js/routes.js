import Verstappen from './components/Verstappen.vue';
import NotFound from './components/NotFound.vue';
import Homepage from './components/Homepage.vue';
import Standen from './components/Standen.vue';
import Nieuws from './components/Nieuws.vue';

export default {
	mode: 'history',

	routes: [
		{
			path: '/',
			component: Homepage,
			name: 'homepage'
		},
		{
			path: '/nieuws',
			component: Nieuws,
			name: 'nieuws'
		},
		{
			path: '/verstappen',
			component: Verstappen,
			name: 'verstappen'
		},
		{
			path: '/standen',
			component: Standen,
			name: 'standen'
		},
		{
			path: '*',
			component: NotFound,
		},
	]
}
