## Eindexamen FRO

Live website: https://formule1vandaag.nl/

Start Document: https://drive.google.com/file/d/1a-hswtGkpeAmH5vtOkjVdvZw3WmTPR3D/view?usp=sharing

Test Rapport Responsive Design: https://docs.google.com/document/d/1cigKMe348W5atn-iCtFeUSi89QO33gvrCoLPOcjXx6Y/edit?usp=sharing

Test Rapport Faulty Images: https://docs.google.com/document/d/17bnlwY2EDZiam4JbYdmJVmA_XIpY8w1qecIscjKAMgo/edit?usp=sharing

SEO Rapport: https://docs.google.com/document/d/1I_sI8ybsvABiQnU782SaYl4ZeM_QhuxgT7CVS_xhc6g/edit?usp=sharing

User Stories: https://docs.google.com/document/d/1rrXlYS6J4ai_jq8FMzVpEl_54AaplM2Y9wPkYeiWI_4/edit?usp=sharing

Gemaakt in: Laravel, Vue.js en Tailwind.

## Vue.js (Router) information

Vue.js heeft de volgende componenten in de HTML staan:

<homepage></homepage>
<navbar></navbar> (dus geen <nav> html element)
<nieuws></nieuws>
<notfound></notfound>
<standen></standen>
<verstappen></verstappen>
<vuefooter></vuefooter>

## Router information

Vue Router GEEFT syntax errors in W3 Schools validator. Dit is omdat W3 Schools alleen STANDAARD HTML support en geen packages zoals Vue.js. Net zoals Vue.js language support en haar <style></style>. Ook herkent W3 Schools validator geen custom componenten zoals <navbar></navbar>. En dus ook niet de routes die je in Vue.js Router moet definiëren.

## Laravel information
Ook werd er tijdens mijn examen gevraagd waarom er "bootstrap" in mijn examen zat. Dit klopt niet, dit is Tailwind. Wel is er een "bootstrap.php" file in de repo, die laad alle packages in, zoals vue.js, CSRF tokens in de header en Lodash. Dit heeft Laravel NODIG.
Ook werd er verteld dat er onnodige bestanden in mijn repo zat, hier kan ik niets aan doen omdat dit een framework is en crasht wanneer er bestanden worden verwijderd van de basis waar de applicatie op draait.

## General information & changes
Nadat ik hoorde dat ik niet was geslaagd voor mijn front-end examen heb ik gehoord dat mijn git commits net wekelijks was (nu wel) en ik geen SEO/Testen had gemaakt. Dit is nu wel het geval. Verder is het project identiek aan het project die ik heb laten zien tijdens mijn presentatie.


